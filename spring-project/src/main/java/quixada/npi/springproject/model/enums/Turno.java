package quixada.npi.springproject.model.enums;

public enum Turno {
	MANHA, TARDE, NOITE;
}
