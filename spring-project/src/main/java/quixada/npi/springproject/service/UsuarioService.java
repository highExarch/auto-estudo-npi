package quixada.npi.springproject.service;


import quixada.npi.springproject.model.Usuario;

import java.util.List;
import java.util.Optional;

public interface UsuarioService {

    Optional<Usuario> findById(int id);

    Usuario findByEmail(String email);

    List<Usuario> findAll();

    Usuario registerUsuario(Usuario u);

    boolean removeUsuario(int id);

    Usuario updateUsuario(Usuario newUser);

    List<Usuario> findAllUsuariosActive();
    
}
