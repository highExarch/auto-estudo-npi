package quixada.npi.springproject.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import quixada.npi.springproject.model.Usuario;
import quixada.npi.springproject.repository.UsuarioRepository;
import quixada.npi.springproject.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Usuario findByEmail(final String email) {
		return usuarioRepository.findByEmail(email);
	}

	@Override
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario registerUsuario(final Usuario u) {
		u.setPassword(new BCryptPasswordEncoder().encode(u.getPassword()));
		return usuarioRepository.save(u);
	}

	@Override
	public boolean removeUsuario(final int id) {
		if (findAllUsuariosActive().stream().noneMatch(user -> user.getId() == id)) {
			usuarioRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Usuario updateUsuario(final Usuario newUser) {
		return usuarioRepository.save(newUser);
	}

	@Override
	public List<Usuario> findAllUsuariosActive() {
		return usuarioRepository.findAll().stream().filter(user -> user.isHabilitado() == true)
				.collect(Collectors.toList());
	}

	@Override
	public Optional<Usuario> findById(final int id) {
		return usuarioRepository.findById(id);
	}

}
