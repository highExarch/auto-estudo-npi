package quixada.npi.springproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import quixada.npi.springproject.model.Curso;
import quixada.npi.springproject.repository.CursoRepository;

@RestController
@RequestMapping("/cursos")
public class CursoController {
	@Autowired
	private CursoRepository cursoRepo;
	
	@GetMapping
	public ResponseEntity<List<Curso>> getAllCursos() {
		return ResponseEntity.ok(cursoRepo.findAll());
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Curso> getOneCurso(@PathVariable final long id) {
		return ResponseEntity.ok(cursoRepo.getOne(id));
	}
	
	@PostMapping("")
	public ResponseEntity<Curso> registerCurso(@RequestBody Curso curso) {
		return ResponseEntity.ok(cursoRepo.save(curso));
	}
	
	@PutMapping("")
	public ResponseEntity<Curso> updateCurso(@RequestBody Curso curso) {
		return ResponseEntity.ok(cursoRepo.save(curso));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Curso> registerCurso(@PathVariable final long id) {
		cursoRepo.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
