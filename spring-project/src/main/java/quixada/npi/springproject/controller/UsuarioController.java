package quixada.npi.springproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import quixada.npi.springproject.exception.StandardError;
import quixada.npi.springproject.model.Usuario;
import quixada.npi.springproject.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("")
    public ResponseEntity<List<Usuario>> findAll() {
        return ResponseEntity.ok(usuarioService.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Optional<Usuario>> find(@PathVariable final Integer id) {
        return ResponseEntity.ok(usuarioService.findById(id));
    }

    @PostMapping("/cadastro")
    public ResponseEntity<Usuario> create(@RequestBody final Usuario usuario) {
        return new ResponseEntity<Usuario>(usuarioService.registerUsuario(usuario), HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteUsuario(@PathVariable final Integer id) {
        if (usuarioService.removeUsuario(id)) {
            return ResponseEntity.ok().build();
        }
        return new ResponseEntity<>(new StandardError(500, "Usuário ainda ativo", "", "/usuario/:id"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/atualizar")
    public ResponseEntity<Usuario> updateUsuario(@RequestBody final Usuario newUsuario) {
        return new ResponseEntity<>(usuarioService.updateUsuario(newUsuario), HttpStatus.OK);
    }
}
