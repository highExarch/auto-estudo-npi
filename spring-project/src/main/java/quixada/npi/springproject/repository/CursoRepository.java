package quixada.npi.springproject.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import quixada.npi.springproject.model.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {
	Optional<Curso> findById(Long id);
}
