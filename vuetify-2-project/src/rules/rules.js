const Rules = {
  requiredRule: [v => !!v || "Campo obrigatório"],
  emailRules: [
    v => !!v || "E-mail obrigatório",
    v => /.+@.+\..+/.test(v) || "E-mail precisa ser válido"
  ]
};

export default Rules;
