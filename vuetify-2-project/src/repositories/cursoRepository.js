import Repository from "./Repository";

const resource = "/cursos";

export default {
    getByAutenticacao() {
        return Repository.get(`${resource}/curso`);
    },

    getAll() {
      return Repository.get(`${resource}`);
    }
}
