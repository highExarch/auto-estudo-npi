import store from "../store";
import LoginPage from "../views/Login";
import Home from "../views/Home";
import Register from "../views/Register";
import Courses from '../views/Courses';

// import Teste from '../views/Teste';

export default [
  /* Geral */
  {
    path: "/",
    name: "Home",
    component: Home
  },
  // {
  //   path: '/teste',
  //   name: "Teste",
  //   component: Teste
  // },
  {
    path: "/courses",
    name: "Courses",
    component: Courses,
  },
  {
    path: "/login",
    meta: {
      public: true
    },
    props: true,
    name: "Login",
    component: LoginPage
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  },
  {
    path: "/logout",
    meta: { breadcrumb: true },
    name: "Logout",
    beforeEnter(to, from, next) {
      store.dispatch("auth/removeToken", false);
      next("/login");
    }
  }
];
